﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace SendMsmqMessagesToClient
{
    class Program
    {
        static readonly string QUEUE_NAME = @".\private$\queue_orch_msg";
        static readonly string MESSAGE = "Chegou uma nova atualização de Fedds!";
        static readonly int TIMER_INTERVAL = 15000;
        static Timer _timer;        

        static void Main(string[] args)
        {           
            CreateQueue();
            StartMessageProducer();
            Initialize();
        }

        static void Initialize()
        {
            Console.WriteLine("Starting MSMQ Messages Producer.");
            Console.WriteLine("-------------------------------");
            Console.WriteLine("");
            Console.ReadKey();               
        }

        static void CreateQueue()
        {
            using (MessageQueue messageQueue = new MessageQueue())
            {
                messageQueue.Path = QUEUE_NAME;

                if (!MessageQueue.Exists(messageQueue.Path))
                    MessageQueue.Create(messageQueue.Path);                
            }
        }

        static void StartMessageProducer()
        {
            _timer = new Timer(TIMER_INTERVAL);
            _timer.Elapsed += Timer_Elapsed;
            _timer.AutoReset = true;
            _timer.Enabled = true;
        }

        static void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Console.WriteLine("Starting Sending message to {0} at {1}", "queue_orch_msg", e.SignalTime);
            Console.WriteLine("-----------------------------------------------------------------------");
            Console.WriteLine("");
            SendMessage(MESSAGE);
        }

        static void SendMessage(string message)
        {                       
            using (MessageQueue messageQueue = new MessageQueue())
            {
                messageQueue.Path = QUEUE_NAME;
                messageQueue.Send(message);
            }
        }
    }
}
