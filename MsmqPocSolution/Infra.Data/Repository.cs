﻿using DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;

namespace Infra.Data
{
    public class Repository
    {
        public LogDTO RegisterFundPositionList()
        {
            var log = new LogDTO();
            
            log.Mediator = "TB_FUNDS_POS";
            log.DataRegistro = DateTime.Now.ToString();

            SqlConnection cnn = 
                new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=FMS;Integrated Security=True;Connect Timeout=30;");

            cnn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cnn;
            cmd.Parameters.AddWithValue("@Mediator", log.Mediator);
            cmd.Parameters.AddWithValue("@DataRegistro", log.DataRegistro);            
            cmd.CommandText = "insert into dbo.[Log] (Mediator, DataRegistro) values (@Mediator, @DataRegistro)";
            log.LogId = cmd.ExecuteNonQuery();

            var funds = GetFundsPositionList();
            foreach (var fund in funds)
            {
                SqlCommand cmd2 = new SqlCommand();
                cmd2.Connection = cnn;
                cmd2.Parameters.AddWithValue("@ProductId", fund.ProductId);
                cmd2.Parameters.AddWithValue("@ProductName", fund.ProductName);
                cmd2.Parameters.AddWithValue("@GrossEquityValue", fund.GrossEquityValue);
                cmd2.Parameters.AddWithValue("@QuotaEquityValue", fund.QuotaEquityValue);
                cmd2.Parameters.AddWithValue("@ManagerialEquityValue", fund.ManagerialEquityValue);
                cmd2.Parameters.AddWithValue("@NavEquityValue", fund.NavEquityValue);
                cmd2.Parameters.AddWithValue("@LogId", log.LogId);
                
                cmd2.CommandText = 
                    @"insert into dbo.[FundsPosition] 
                       values (@ProductId, @ProductName, @GrossEquityValue, @QuotaEquityValue, @ManagerialEquityValue, @NavEquityValue, @LogId)";
                
                cmd2.ExecuteNonQuery();
            }

            return log;
        }

        private List<FundDTO> GetFundsPositionList()
        {
            var list = new List<FundDTO>();

            list.Add(new FundDTO
            {
                ProductId = 1,
                ProductName = "FUNDO 1",
                GrossEquityValue = new Random(1000).Next(),
                QuotaEquityValue = new Random(5000).Next(),
                NavEquityValue = new Random(42343).Next(),
                ManagerialEquityValue = new Random(43323).Next()
            });

            list.Add(new FundDTO
            {
                ProductId = 5,
                ProductName = "FUNDO 2",
                GrossEquityValue = new Random(33232).Next(),
                QuotaEquityValue = new Random(21212).Next(),
                NavEquityValue = new Random(212121).Next(),
                ManagerialEquityValue = new Random(21212).Next()
            });

            list.Add(new FundDTO
            {
                ProductId = 10,
                ProductName = "FUNDO 3",
                GrossEquityValue = new Random(99995).Next(),
                QuotaEquityValue = new Random(23654).Next(),
                NavEquityValue = new Random(15472).Next(),
                ManagerialEquityValue = new Random(336543).Next()
            });

            list.Add(new FundDTO
            {
                ProductId = 52,
                ProductName = "FUNDO 4",
                GrossEquityValue = new Random(121323).Next(),
                QuotaEquityValue = new Random(43434).Next(),
                NavEquityValue = new Random(55642).Next(),
                ManagerialEquityValue = new Random(33654).Next()
            });

            list.Add(new FundDTO
            {
                ProductId = 14,
                ProductName = "FUNDO 5",
                GrossEquityValue = new Random(5435).Next(),
                QuotaEquityValue = new Random(212132).Next(),
                NavEquityValue = new Random(342343).Next(),
                ManagerialEquityValue = new Random(66542).Next()
            });

            list.Add(new FundDTO
            {
                ProductId = 99,
                ProductName = "FUNDO 6",
                GrossEquityValue = new Random(867867).Next(),
                QuotaEquityValue = new Random(5445).Next(),
                NavEquityValue = new Random(54545).Next(),
                ManagerialEquityValue = new Random(2121).Next()
            });

            return list;
        }

        private double GetDoubleRandomValue()
        {
            Random _random = new Random();
            return _random.Next();
        }
    }
}
