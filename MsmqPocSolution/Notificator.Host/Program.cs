﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notificator.Host
{
    class Program
    {
        static void Main(string[] args)
        {
            Random random = new Random();

            for (int i = 1; i < 100; i++)
            {
                var value = random.NextDouble() * 99999999 * 0.0323;
                Console.WriteLine(Math.Round(value, 2));
            }
            
            Console.ReadKey();
        }
    }
}
