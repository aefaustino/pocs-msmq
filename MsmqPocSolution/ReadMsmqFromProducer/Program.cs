﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ReadMsmqFromProducer
{
    class Program
    {
        static readonly string QUEUE_NAME = @".\private$\queue_orch_msg";

        static void Main(string[] args)
        {
            Initialize();
            MonitorQueueOrch();            
            Console.ReadKey();
        }

        static void Initialize()
        {
            Console.WriteLine("Starting MSMQ Messages Consumer.");
            Console.WriteLine("-------------------------------");
            Console.WriteLine("");            
        }

        static void MonitorQueueOrch()
        {
            using (MessageQueue queueOrch = new MessageQueue(QUEUE_NAME))
            {
                while (true)
                {
                    var queueMessage = queueOrch.GetMessageEnumerator2();
                    if (queueMessage.MoveNext())
                    {
                        Thread.Sleep(10000);
                        Message message = queueOrch.Receive();
                        message.Formatter = new XmlMessageFormatter(new string[] { "System.String" });
                        ShowMessage(message.Body.ToString());                        
                    }                    
                }
            }
        }

        static void ShowMessage(string message)
        {
            Console.WriteLine("Message received at {0}", DateTime.Now);
            Console.WriteLine(message);
            Console.WriteLine("-----------------------------------------------------------------------");
            Console.WriteLine("");
        }
    }
}
