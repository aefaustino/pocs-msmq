﻿using Mediator;
using System;
using System.Messaging;
using System.Windows.Forms;

namespace WinFormMsmqTest01
{
    public partial class Form1 : Form
    {
        private FundPositionMediator _FundPositionMediator;

        public Form1()
        {
            _FundPositionMediator = new FundPositionMediator();

            InitializeComponent();
        }

        private void SendButton_Click(object sender, EventArgs e)
        {
            using (MessageQueue messageQueue = new MessageQueue())
            {
                messageQueue.Path = @".\private$\msmq_teste";

                if (!MessageQueue.Exists(messageQueue.Path))
                    MessageQueue.Create(messageQueue.Path);

                System.Messaging.Message message = new System.Messaging.Message();
                message.Body = txtSendMessage.Text;
                messageQueue.Send(message);
            }
        }

        private void ReceiveButton_Click(object sender, EventArgs e)
        {
            using (MessageQueue messageQueue = new MessageQueue())
            {
                messageQueue.Path = @".\private$\msmq_teste";
                System.Messaging.Message message = new System.Messaging.Message();
                message = messageQueue.Receive(new TimeSpan(0, 0, 5));
                message.Formatter = new XmlMessageFormatter(new string[] { "System.String" });
                string msg = message.Body.ToString();
                txtReceiveMessage.Text = msg;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var result =
                _FundPositionMediator.ProcessFundPosition();

            MessageBox.Show(result.Mediator + "processed!");
        }
    }
}
