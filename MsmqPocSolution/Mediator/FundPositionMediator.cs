﻿using Infra.Data;
using DataTransferObjects;


namespace Mediator
{
    public class FundPositionMediator
    {
        private Repository _repository;

        public FundPositionMediator()
        {
            _repository = new Repository();
        }

        public LogDTO ProcessFundPosition()
        {
            return _repository.RegisterFundPositionList();
        }
    }
}
