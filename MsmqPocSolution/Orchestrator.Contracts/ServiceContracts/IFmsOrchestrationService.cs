﻿using Orchestrator.Contracts.DataContracts;
using System.ServiceModel;

namespace Orchestrator.Contracts.ServiceContracts
{
    [ServiceContract]
    public interface IFmsOrchestrationService
    {
        [OperationContract(IsOneWay = true)]
        void ProcessFundsPosition(ProcessFundsPositionRequest request);

        //void ProcessFundsPosition
    }
}
