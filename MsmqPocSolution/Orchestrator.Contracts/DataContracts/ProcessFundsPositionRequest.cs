﻿using System.Runtime.Serialization;

namespace Orchestrator.Contracts.DataContracts
{
    [DataContract]
    public class ProcessFundsPositionRequest
    {
        [DataMember]
        public string ProductName { get; set; }
    }
}
