﻿using System;
using Mediator;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        private FundPositionMediator _FundPositionMediator;

        public UnitTest1()
        {
            _FundPositionMediator = new FundPositionMediator();
        }

        [TestMethod]
        public void TestMethod1()
        {
            var result =
                _FundPositionMediator.ProcessFundPosition();

            Assert.IsTrue(result.LogId > 0);
        }
    }
}
