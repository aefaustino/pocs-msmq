﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransferObjects
{
    public class LogDTO
    {
        public long LogId { get; set; }

        public string Mediator { get; set; }

        public string DataRegistro { get; set; }
    }
}
