﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransferObjects
{
    public class FundDTO
    {
        public int ProductId { get; set; }

        public string ProductName { get; set; }

        public double? GrossEquityValue { get; set; }

        public double? QuotaEquityValue { get; set; }

        public double? ManagerialEquityValue { get; set; }

        public double? NavEquityValue { get; set; }

        public long LogId { get; set; }
    }
}
