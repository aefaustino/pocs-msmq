﻿CREATE DATABASE FMS;
go

use FMS;
go

CREATE TABLE [dbo].[FundsPosition] (
    [ProductId]             INT             NOT NULL,
    [ProductName]           VARCHAR (50)    NOT NULL,
    [GrossEquityValue]      DECIMAL (18, 6) NULL,
    [QuotaEquityValue]      DECIMAL (18, 6) NULL,
    [ManagerialEquityValue] DECIMAL (18, 6) NULL,
    [NavEquityValue]        DECIMAL (18, 6) NULL,
    [LogId]                 BIGINT          NULL
)
go

CREATE TABLE [dbo].[Log] (
    [LogId]        BIGINT PRIMARY KEY IDENTITY        NOT NULL,
    [Mediator]     VARCHAR (50) NOT NULL,
    [DataRegistro] VARCHAR (50) NOT NULL
)

go